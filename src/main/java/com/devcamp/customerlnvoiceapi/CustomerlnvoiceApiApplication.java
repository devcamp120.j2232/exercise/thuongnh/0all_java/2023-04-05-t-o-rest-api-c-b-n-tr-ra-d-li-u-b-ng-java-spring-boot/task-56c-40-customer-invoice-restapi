package com.devcamp.customerlnvoiceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerlnvoiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerlnvoiceApiApplication.class, args);
	}

}
